Exact coordinates of a wave attractor (n,1)

A python function for calculation of the coordinates of a wave attactor (n,1)
according to the paper: I.Sibgatullin, A.Petrov, X.Xu, L.Maas
		        On (n,1) wave attractors: coordinates and saturation time. 
                        https://doi.org/10.3390/sym14020319

Input: n, d, tau - geometric parameters of a (n,1) wave attractor.
d and tou have to lie in an area of exisence of a wave attractor (n,1) according to the formulae (7) of the paper.
Output: coordinates - a numpy array of coordinates of the reflection points.

