% matlab/octave translation of the python function for calculation of 
% the coordinates of attractor (n,1)
% based on the paper: I.Sibgatullin, A.Petrov, X.Xu, L.Maas
%                     On (n,1) wave attractors: coordinates and saturation time. 
% numbering in python code starts with 0 point, located at the slope, 
% according to the paper above.
% matlab/octave code requires array numbers to start from 1,
% hence the explicite multiple occurence of "+1" in the code.

n=3
d=0.5
tau=0.62
len=n*2+2;
coordx = ones(len,1);
coordy = zeros(len,1); 

coordx(0 +1) = n*tau - 1;
coordy(0 +1) = (-n*tau^2+2*tau) / (1-d);
coordx(1 +1) = coordx(0 +1) - (tau - coordy(0 +1)); 
coordy(1 +1) = tau;

for k = 2 +1 : n+1
        coordx(k ) = coordx(1 +1) - (k-1 -1) * tau ; 
        coordy(k ) = ( (-1)^(k+1 +1) + 1)/2*tau ;
end;
    coordx(n+1  +1)  = -1;
    coordy(n+1  +1) = (rem(n,2)==0)*coordy(0 +1) + (rem(n,2)==1) * (tau-coordy(0 +1));
    coordx(n+2  +1) = (rem(n,2)==0)*(-1 + tau - coordy(n+1 +1)) + (rem(n,2)==1) * (-1+coordy(n+1 +1));
    coordy(n+2  +1) = (rem(n,2)==0)*(  tau ) + (rem(n,2)==1) * (0);
for k = n+3 +1:len
            coordx(k) = coordx(n+2 +1) + (k-2-n -1) * tau;
            coordy(k) = ( (-1)^(k+1) + 1)/2*tau; 
end;
tankx = [-1,1,d,-1,-1]; tanky = [0.0,0.0,tau,tau,0]; % coordinates of the domain
plot(tankx,tanky,'k','LineWidth',8)
hold on
plot(coordx(1:end),coordy(1:end),'ro-','LineWidth',7, 'MarkerFaceColor', 'r', 'MarkerSize', 6)
axis equal
